<?php
	include_once 'backend/includes/helpers.php';
	include_once 'names.php';

	// check if a specific video has been requested
	if (isset($_GET['video'])) {
		// this should be fine for most cases
		$video = identifierToFileData($_GET['video']);

		// but if it isn't we can try this too
		if ($video === false) {
			// get raw query so it doesn't try to parse the reserved characters (;/?:@&=+,$)
			// the `substr` call removes the "video=" from the start
			$identifier = substr($_SERVER['QUERY_STRING'],6);
			$video = identifierToFileData(rawurldecode($identifier));
		}

		if ($video !== false) {
			$series = $video['series'];
			$title = $video['title'];
			$filename = $video['file'];
			if($ADD_KARAOKE_INFO_LINK) $uid = $video['uid'];
			$pagetitle = (isset($video['egg']) ? 'Secret~' : I18N::t('{title} from {series}', ['{title}' => $title, '{series}' => $series,]));
			$description = '';
		} else {
			header('HTTP/1.0 404 Not Found');
			include_once 'backend/pages/notfound.php';
			die();
		}
	} else { // Otherwise pick a random video

		// Remove 18+ if they weren't requested.
		if (!isset($_GET['r18'])) {
			foreach ($names as $series => $video_array) {
				foreach ($video_array as $title => $data)
					if (isset($data['r18']) && $data['r18'])
						unset($names[$series][$title]);
				if (!$names[$series])
					unset($names[$series]);
			}
		}

		$series = array_rand($names);
		$title = array_rand($names[$series]);
		$video = $names[$series][$title];
		$filename = $video['file'];
		if($ADD_KARAOKE_INFO_LINK) $uid = $video['uid'];
		$pagetitle = 'Karaoke Mugen Live!';
		$description = I18N::t('Random Karaokes in your browser');
	}

	if($USE_UID_AS_IDENTIFIER)
		$identifier = $uid;
	else
		$identifier = filenameToIdentifier($filename);

	$filename = rawurlencode($filename);

	$songwriters = [];
	$songKnown = array_key_exists('song', $video);
	if ($songKnown) {
		$songTitle = $video['song']['title'];
		$songArtist = $video['song']['artist'];
		$songwriters = empty($video['song']['songwriters'])? []:$video['song']['songwriters'];
		if (empty($description)) {
			$description = I18N::t('Song: "{title}" by {artist}', ['{title}' => $songTitle, '{artist}' => $songArtist,]);
		}
	}

	$subtitlesAvailable = array_key_exists('subtitles', $video);
	$subtitleAttribution = $subtitlesAvailable ? $video['subtitles'] : '';

	$isEgg = isset($video['egg']);

	$baseURL = 'https://' . $WEBSITE_URL . substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/') + 1);
	$oembedURL = $baseURL . 'api/oembed/?url=' . $baseURL . '?' . $_SERVER['QUERY_STRING'];
?>
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
	<head>
		<!-- Basic Page Stuff -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo htmlspecialchars($pagetitle); ?></title>
		<meta name="description" content="<?php echo htmlspecialchars($description); ?>">

		<!-- oEmbed Discovery -->
		<link rel="alternate" type="application/json+oembed" href="<?php echo $oembedURL; ?>&format=json" title="<?php echo htmlspecialchars($pagetitle); ?>">
		<link rel="alternate" type="text/xml+oembed" href="<?php echo $oembedURL; ?>&format=xml" title="<?php echo htmlspecialchars($pagetitle); ?>">

        <!-- Twitter meta tags -->
        <meta property="twitter:card" content="player">
        <meta property="twitter:title" content="<?php echo htmlspecialchars($pagetitle); ?>">
        <meta property="twitter:site" content="@KaraokeMugen">
        <meta property="twitter:player" content="<?php echo $baseURL.'?video='.urlencode($identifier) ?>">
        <meta property="twitter:description" content="<?php echo htmlspecialchars($description); ?>">
        <meta property="twitter:player:height" content="720">
        <meta property="twitter:player:width" content="1280">
        <meta property="twitter:image" content="<?php echo $baseURL; ?>assets/logo/512px.png">
        <meta property="twitter:image:alt" content="Logo de l'application">

		<!-- Open Graph Tags -->
		<meta property="og:type" content="video.other">
		<meta property="og:url" content="<?php echo $baseURL . '?video=' . $identifier; ?>">
		<meta property="og:title" content="<?php echo htmlspecialchars($pagetitle); ?>">
		<meta property="og:description" content="<?php echo htmlspecialchars($description); ?>">
		<meta property="og:image" content="<?php echo $baseURL.$DEFAULT_BACKGROUND_IMAGE; ?>"><?php
			/*// also use it to check if we have an audio only file
			$isAudioOnly = true;
			foreach ($video['mime'] as $mime) {
				$ext = mimeToExt($mime);
				$content = ' content="' . $baseURL . 'video/' . $filename . $ext . '"';
				$metaMime = 'video';
				
				if(false !== strpos($mime, 'audio/')) {
					$metaMime = 'audio';
				} else {
					$isAudioOnly = false; // if we have at very least ONE video source, we'll use it.
				}
				echo "\n\t\t" . '<meta property="og:'.$metaMime.'"' . $content . '>';
				echo "\n\t\t" . '<meta property="og:'.$metaMime.':url"' . $content . '>';
				echo "\n\t\t" . '<meta property="og:'.$metaMime.':secure_url"' . $content . '>';
				echo "\n\t\t" . '<meta property="og:'.$metaMime.':type" content="' . htmlspecialchars($mime) . '">';
			}
			echo PHP_EOL;*/
		?>

		<!-- Facebook App Link -->
		<meta property="al:web:url" content="<?php echo $baseURL . '?video=' . $identifier; ?>">

		<!-- CSS and JS Resources -->
		<link rel="stylesheet" type="text/css" href="CSS/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="CSS/main.css">
		<link rel="stylesheet" type="text/css" href="CSS/fonts.css">
		<link rel="stylesheet" type="text/css" href="CSS/subtitles.css">
		<script type="text/javascript">
			// Set config values from PHP into JavaScript.
			window.config = {
				VIDEO_INDEX_PADDING: <?php echo $VIDEO_INDEX_PADDING; ?>,
				USE_FILENAME_AS_IDENTIFIER: <?php echo $USE_FILENAME_AS_IDENTIFIER ? 'true' : 'false'; ?>,
				ADD_KARAOKE_INFO_LINK: <?php echo $ADD_KARAOKE_INFO_LINK ? 'true' : 'false'; ?>,
				USE_UID_AS_IDENTIFIER: <?php echo $USE_UID_AS_IDENTIFIER ? 'true' : 'false'; ?>,
				DEFAULT_BACKGROUND_IMAGE: '<?php echo htmlspecialchars($DEFAULT_BACKGROUND_IMAGE); ?>',
				AUDIO_COVERS_BACKGROUND: <?php echo $AUDIO_COVERS_BACKGROUND ? 'true' : 'false'; ?>
			};

			// noinspection JSAnnotator
            const firstVideo = <?php echo json_encode([
                'title' => $title,
                'source' => $series,
                'file' => $video['file'],
                'mime' => $video['mime'],
                'song' => existsOrDefault('song', $video),
                'gain' => array_key_exists('gain', $video)? (10**($video['gain']/20)):1,
                'subtitles' => existsOrDefault('subtitles', $video),
                'egg' => existsOrDefault('egg', $video),
                'uid' => existsOrDefault('uid', $video)
            ]); ?>;
		</script>
        <script src="JS/lang.js"></script>
        <script src="JS/clipboard.min.js"></script>
		<script src="JS/main.js"></script><?php if($AUDIO_COVERS_BACKGROUND) { ?>
		<script src="JS/mp3tag.min.js"></script><?php } if($AUDIO_COVERS_BACKGROUND || !empty($DEFAULT_BACKGROUND_IMAGE)) { ?>
		<script src="JS/audio-bg.js"></script><?php } ?>
		<script src="JS/fontname.js"></script>
		<script defer src="JS/subtitles.js"></script>

		<!-- Web App Tags -->
		<meta name="theme-color" content="#E58B00">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

		<!-- Logo Links -->
		<link href="/assets/logo/152px.png" rel="apple-touch-icon">
		<link href="/assets/logo/16px.png" rel="icon" sizes="16x16">
		<link href="/assets/logo/32px.png" rel="icon" sizes="32x32">
		<link href="/assets/logo/64px.png" rel="icon" sizes="64x64">
		<link href="/assets/logo/152px.png" rel="icon" sizes="152x152">
		<link href="/assets/logo/512px.png" rel="icon" sizes="512x512">
	</head>
	<body>
		<div id="wrapper"<?php if(!empty($DEFAULT_BACKGROUND_IMAGE)) { ?>
						style="background-image: url('<?php echo htmlspecialchars($DEFAULT_BACKGROUND_IMAGE); ?>'); background-size: <?php echo $isAudioOnly? 'contain':'0px'; ?>; background-repeat: no-repeat; background-position: center;"<?php } ?>>
				<video id="bgvid" loop preload="none" playsinline><?php
                                foreach ($video['mime'] as $mime) {
                                        $ext = mimeToExt($mime);
                                        echo "\n\t\t\t\t" . '<source src="video/' . $filename . $ext . '" type="' . htmlspecialchars($mime) . '">';
                                }
                                echo PHP_EOL;
                ?></video>
		</div>

		<i id="giant-play-button" class="overlay fa fa-play quadbutton quadNotMobile"></i>

		<div id="progressbar" class="progressbar">
			<div class="progress">
				<div id="bufferprogress" class="progress"></div>
				<div id="timeprogress" class="progress"></div>
			</div>
		</div>

		<div class="displayTopRight"></div>

		<a id="menubutton" href="/hub/faq.php" class="quadbutton fa fa-bars"></a>
		<a id="searchbutton" href="/list/" class="quadbutton fa fa-search"></a>

		<div class="controlsleft">
			<span id="getnewvideo" class="quadbutton fa fa-sync-alt"></span>
			<span id="autonext" class="quadbutton fa fa-toggle-off"></span>
		</div>

		<div class="controlsright">
			<span id="subtitles-button" class="quadbutton fa fa-comment"<?php if (!$subtitlesAvailable) echo ' style="display:none"'; ?>></span>
			<span id="skip-left" class="quadbutton fa fa-arrow-left"></span>
			<span id="skip-right" class="quadbutton fa fa-arrow-right"></span>
			<span id="pause-button" class="quadbutton fa fa-play"></span>
			<span id="fullscreen-button" class="quadbutton fa fa-expand"></span>
		</div>

		<div id="site-menu" hidden>
			<span id="closemenubutton" class="quadbutton fa fa-times"></span>

			<p id="title"><?php echo $title; ?> </p>
			<p id="source"><?php echo I18N::t('From {series}', ['{series}' => '<em>'.htmlspecialchars($series).'</em>']); ?></p>
			<p id="songwriters"><span class="fa fa-signature"></span><?php if(!empty($songwriters)) { echo I18N::t('Composed by: {songwriters}', ['{songwriters}' => implode(', ', $songwriters)]); } ?></p>
			<p id="uid" hidden><?php echo $uid ?></p>
			<p id="song"><span class="fa fa-microphone-alt"></span><?php // If we have the data, echo it
				if ($songKnown)
					echo I18N::t("Sung by {artist}", ['{artist}' => htmlspecialchars($songArtist)]);
				else { // Otherwise, let's just pretend it never existed... or troll the user.
					if ($isEgg || mt_rand(0,100) == 1)
						echo I18N::t("Sung by {artist}", ['{artist}' => 'Darude']);
				} ?></p>
			<p id="subs"<?php if (!$subtitlesAvailable) echo ' style="display:none"'; ?>><span class="fa fa-user-secret"></span><?php echo I18N::t('Karaoke created by {attribution}', ['{attribution}' => '<span id="subtitle-attribution">'.$subtitleAttribution.'</span>']); ?></p>

			<ul id="linkarea">
				<li class="link"<?php if ($isEgg) echo ' hidden'; ?>><div class="le" data-clipboard-text="https://<?php echo $WEBSITE_URL; ?>/?video=<?php if (!$isEgg) echo $identifier; ?>" id="videolink"><span class="fa fa-link"></span><?php echo I18N::t('Copy link to this video'); ?></div></li><?php
					foreach ($video['mime'] as $mime) {
					    if (strpos($mime, 'audio/') === FALSE) {
                            $ext = mimeToExt($mime);
                            echo "\n\t\t\t\t" . '<li class="link videodownload"' . ($isEgg ? ' hidden' : '') . '><a href="video/' . (!$isEgg ? $filename . $ext : '') . '" download> <span class="fa fa-file-video"></span>' . I18N::t('Download this video as {format}', ['{format}' => substr($ext,1)]) . '</a></li>';
                        } else {
                            $ext = mimeToExt($mime);
                            echo "\n\t\t\t\t" . '<li class="link videodownload"' . ($isEgg ? ' hidden' : '') . '><a href="video/' . (!$isEgg ? $filename . $ext : '') . '" download> <span class="fa fa-music"></span>' . I18N::t('Download this music as {format}', ['{format}' => substr($ext,1)]) . '</a></li>';
                        }
					}
					if (isset($video['subtitles'])) {
						echo "\n\t\t\t\t" . '<li class="link videodownload"' . ($isEgg ? ' hidden' : '') . '><a href="subtitles/' . (!$isEgg ? $filename . '.ass' : '') . '" download> <span class="fa fa-file-alt"></span>' . I18N::t('Download karaoke as {format}', ['{format}' => 'ass']) . '</a></li>';
					}
					echo PHP_EOL;
				?>
				<?php if($ADD_KARAOKE_INFO_LINK) echo "\n\t\t\t\t" . '<li class="link videodownload"><a href="http://kara.moe/base/kara/' . $uid . '" target="_blank"><span class="fa fa-info"></span>' . I18N::t('More infos about this karaoke') . '</a></li>'; ?>
				<li class="link"><a id="listlink" href="/list/"><span class="fa fa-list"></span><?php echo I18N::t('Video list') ?></a></li>
				<li class="link"><a id="listlink" href="http://karaokes.moe"><span class="fa fa-external-link-alt"></span><?php echo I18N::t('Karaoke Mugen Website') ?></a></li>
				<li class="link"><a href="/hub/"><span class="fa fa-home"></span><?php echo I18N::t('Hub') ?></a></li>
			</ul>

			<div class="accordion">
				<input type="checkbox" id="settings-checkbox">
				<label for="settings-checkbox">
					<i class="fa fa-chevron-right"></i>
					<i class="fa fa-chevron-down"></i>
					<?php echo I18N::t('Saved settings') ?>
				</label>
				<form id="settings-form">
					<fieldset>
						<legend><?php echo I18N::t('Show Video Title') ?></legend>
						<input id="show-title-checkbox" type="checkbox" checked><label for="show-title-checkbox"><?php echo I18N::t('Yes') ?></label>
						<label id="show-title-delay"><?php echo I18N::t('after {input} seconds', ['{input}' => '<input type="number" min="0" value="0" step="1">']) ?></label>
					</fieldset>
					<fieldset>
						<legend><?php echo I18N::t('On End') ?></legend>
						<label><input name="autonext" type="radio" value="false"><?php echo I18N::t('Repeat Video') ?></label>
						<label><input checked name="autonext" type="radio" value="true"><?php echo I18N::t('Get a New Video') ?></label>
					</fieldset>
					<fieldset>
						<legend><?php echo I18N::t('Subtitles') ?></legend>
						<label><input checked id="subtitle-checkbox" type="checkbox"><?php echo I18N::t('Yes') ?></label>
					</fieldset>
					<fieldset>
						<legend><?php echo I18N::t('Volume') ?></legend>
						<input id="volume-slider" type="range" min="0" max="100" value="100" step="1">
						<label for="volume-slider" id="volume-amount">100%</label>
					</fieldset>
				</form>
			</div>

			<div class="accordion">
				<input type="checkbox" id="keybindings-checkbox">
				<label for="keybindings-checkbox">
					<i class="fa fa-chevron-right"></i>
					<i class="fa fa-chevron-down"></i>
                    <?php echo I18N::t('Keyboard bindings') ?>
				</label>
				<table id="keybindings-table">
					<tr><th><?php echo I18N::t('Key') ?></th><th><?php echo I18N::t('Action') ?></th></tr>
					<tr><td>M</td><td><?php echo I18N::t('Open/Close Menu') ?></td></tr>
					<tr><td>/</td><td><?php echo I18N::t('Open Search Pane') ?></td></tr>
					<tr><td>N</td><td><?php echo I18N::t('Get a new video') ?></td></tr>
					<tr><td>S</td><td><?php echo I18N::t('Toggle subtitles (if available)') ?></td></tr>
					<tr><td><span class="fa fa-arrow-left"></span>/<span class="fa fa-arrow-right"></span></td><td><?php echo I18N::t('Back/Forward 10 seconds') ?></td></tr>
					<tr><td><?php echo I18N::t('Space') ?></td><td><?php echo I18N::t('Pause/Play') ?></td></tr>
					<tr><td>F</td><td><?php echo I18N::t('Toggle fullscreen') ?></td></tr>
					<tr><td><?php echo I18N::t('Page Up/Down') ?></td><td><?php echo I18N::t('Volume') ?></td></tr>
					<tr><td><?php echo I18N::t('Scroll Wheel') ?></td><td><?php echo I18N::t('Volume') ?></td></tr>
				</table>
			</div>

            <div class="accordion">
                <input type="checkbox" id="language-checkbox">
                <label for="language-checkbox">
                    <i class="fa fa-chevron-right"></i>
                    <i class="fa fa-chevron-down"></i>
                    <?php echo I18N::t('Language') ?>
                </label>
                <table id="language-table">
                    <tbody>
                        <?php
                            foreach ($APPLANGS as $index => $lang) {
                                if ($index % 4 === 0) {
                                    echo '<tr><td class="link"><a href="https://' . $WEBSITE_URL .'/?lang='.$lang.'">' . locale_get_display_name($lang, $lang) . '</a></td>';
                                } elseif ($index %4 === 3) {
                                    echo '<td class="link"><a href="https://' . $WEBSITE_URL .'/?lang='.$lang.'">' . locale_get_display_name($lang, $lang) . '</a></td></tr>';
                                } else {
                                    echo '<td class="link"><a href="https://' . $WEBSITE_URL .'/?lang='.$lang.'">' . locale_get_display_name($lang, $lang) . '</a></td>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
		</div>

		<div id="tooltip" class="is-hidden"></div>

		<span id="title-popup"></span>
		<div id="modal" class="overlay"><iframe name="_ifSearch"></iframe></div>

		<?php $jsctl = I18N::_('js')->dump();
		if(!empty($jsctl)) {
			echo '<template id="locale">'.json_encode($jsctl).'</template>';
		} ?>

		<?php include_once 'backend/includes/botnet.html'; ?>
	</body>
</html>
