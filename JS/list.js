let list, playlist = [];
let playlistBot;
let playlistContent;
let showPlaylist = false;
let RegExEnabled = false;

let showR18 = (window.location.toString().indexOf('r18') >= 0);

// polyfill for noncompliant browsers
if (!(HTMLCollection.prototype[Symbol.iterator])) HTMLCollection.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];

// If local/session storage isn't available, set it to a blank object. Nothing
// will be stored, but it means we don't have to check every time we use it.
let myLocalStorage, mySessionStorage;
try { myLocalStorage = window.localStorage || {}; }
catch (e) { myLocalStorage = {}; }
try { mySessionStorage = window.sessionStorage || {}; }
catch (e) { mySessionStorage = {}; }

addEventListener("load", setup);

function setup() {
	initLang();
	// get list of series elements and set their id
	list = document.getElementsByClassName("series");
	for (let series of list)
		series.id = series.childNodes[0].nodeValue.normalize("NFKD").replace(/[\u0300-\u036f]/g, "");

	document.getElementById("searchbox").addEventListener("keydown", timerSearchBox);

	// add onclick(addVideoToPlaylist) to fa-plus elements
	const addVideoButtons = document.getElementsByClassName("fa-plus");
	for (let addVideoButton of addVideoButtons) {
		addVideoButton.title = l("Click to add this video to your playlist");
		addVideoButton.addEventListener("click", playlistAdd);
		addVideoButton.nextElementSibling.className = "video";

		// Add 'closed-captioning' icon after videos that have subtitles.
		if (addVideoButton.dataset.subtitles) {
			let newNode = document.createElement("i");
				newNode.className = "fa fa-closed-captioning";
				newNode.title = l("[{author}] subtitles are available for this video", {author: addVideoButton.dataset.subtitles});
			addVideoButton.parentNode.insertBefore(newNode, addVideoButton.nextElementSibling.nextElementSibling);
		}

		// Add 'music' or 'video' icon after videos that we have song info for.
		if (addVideoButton.dataset.songtitle) {
			let newNode = document.createElement("i");
				newNode.className = "fa " + (JSON.parse(addVideoButton.dataset.mime)[0].startsWith('audio/') ? 'fa-music':'fa-video');
				newNode.title = l("{song} by {author}", {song: addVideoButton.dataset.songtitle, author: addVideoButton.dataset.songartist});
			addVideoButton.parentNode.insertBefore(newNode, addVideoButton.nextElementSibling.nextElementSibling);
		}
	}

	// add click events to playlist "menu"
	playlistBot = document.getElementsByClassName('playlistBot')[0];
	playlistBot.children[0].addEventListener("click", editPlaylist);
	playlistBot.children[2].addEventListener("click", startPlaylist);
	if (window.config.USE_UID_AS_IDENTIFIER) playlistBot.children[4].addEventListener("click", exportPlaylist);

	playlistContent = document.getElementsByClassName('playlistContent')[0];

	document.getElementById('playlist-checkbox').addEventListener('click', togglePlaylist);

	// restore the playlist draft from storage if any
	restorePlaylist();
	togglePlaylist();

	// set history state
	history.replaceState("list", document.title);

	//fix for text input focus
	document.getElementById("searchbox").focus();
	let val = document.getElementById("searchbox").value; //store the value of the element
	document.getElementById("searchbox").value = ''; //clear the value of the element
	document.getElementById("searchbox").value = val; //set that value back.
}

let timerVar;
// if no entry is made in the search box for 3 seconds, we submit it.
function timerSearchBox() {
	clearTimeout(timerVar);
	timerVar = setTimeout(function() { document.forms["fmSearch"].submit(); } , 3000);
}

/**
 * Saves the playlist into the local storage for retrieving in next visits
 * @note it's not useful in non-SSR searh
 */
function savePlaylist() {
	myLocalStorage["draft-playlist"] = JSON.stringify(playlist);
	console.log('saving playlist to storage');
}

/**
 * Restores the playlist from the saved playlist in local storage
 * @note it's not useful in non-SSR searh
 */
function restorePlaylist() {
	playlist = myLocalStorage["draft-playlist"] ? JSON.parse(myLocalStorage["draft-playlist"]):[];
	loadPlaylist(true);
	console.log('loading playlist from storage');
}

function refreshPlaylistBox() {
	document.getElementById('playlistTitle').innerHTML = l("{number} Video" + (playlist.length !== 1 ? "s":"") + " in Playlist", {number: playlist.length});
	if (playlist.length !== 0) document.getElementById("playlist").removeAttribute("hidden");
	else document.getElementById("playlist").setAttribute('hidden', '');
}

function cleanPlaylist() {
	playlist = playlist.filter(val => val !== null && val !== undefined);
}

function togglePlaylist() {
	showPlaylist = !showPlaylist;
	if (showPlaylist) {
		document.getElementsByClassName('fa-chevron-down')[0].style.display = 'inherit';
		document.getElementsByClassName('fa-chevron-right')[0].style.display = 'none';
		document.getElementsByClassName('playlistContent')[0].style.display = 'inherit';
	} else {
		document.getElementsByClassName('fa-chevron-down')[0].style.display = 'none';
		document.getElementsByClassName('fa-chevron-right')[0].style.display = 'inherit';
		document.getElementsByClassName('playlistContent')[0].style.display = 'none';
	}
}

/**
 * Export playlist as kmplaylist
 * @note it works only in uid environments
 */
function exportPlaylist() {
	cleanPlaylist();
	const playlist_header = {
		version: 4,
		description: "Karaoke Mugen Playlist File"
	}
	const playlist_information = {
		created_at: new Date().toISOString(),
		modified_at: new Date().toISOString(),
		name: l('Playlist Live'),
		flag_visible: true
	}
	let playlist_contents = [];
	let i = 1;
	for (let video of playlist) {
		playlist_contents.push({
			kid: video.uid,
			nickname: l('A Live User'),
			created_at: new Date().toISOString(),
			pos: i,
			username: l('A Live User'),
			title: video.title
		});
		i++;
	}
	const kmplaylist = {
		Header: playlist_header,
		PlaylistInformation: playlist_information,
		PlaylistContents: playlist_contents
	}
	console.log(kmplaylist);
	let templink = document.createElement('a');
		templink.setAttribute('href', "data:application/json;charset=utf-8," + encodeURIComponent(JSON.stringify(kmplaylist)));
		templink.setAttribute('download', l('Playlist Live')+'.kmplaylist')
		templink.style.display = 'none';
	document.body.appendChild(templink);
	templink.click();
	document.body.removeChild(templink);
}

function playlistAdd() {
	let video;
	if (this.fakeEvent) video = this.video;
	else {
		if (window.config.USE_UID_AS_IDENTIFIER) {
			video = {
				egg: null,
				title: this.nextElementSibling.text,
				source: this.parentElement.parentElement.childNodes[0].nodeValue,
				file: this.dataset.file,
				uid: this.dataset.uid,
				mime: JSON.parse(this.dataset.mime)
			};
		} else {
			video = {
				egg: null,
				title: this.nextElementSibling.text,
				source: this.parentElement.parentElement.childNodes[0].nodeValue,
				file: this.dataset.file,
				mime: JSON.parse(this.dataset.mime)
			};
		}
		if (typeof this.dataset.songtitle === 'string') video.song = {title: this.dataset.songtitle, artist: this.dataset.songartist, songwriters: JSON.parse(this.dataset.songwriters)};
		if (typeof this.dataset.subtitles === 'string') video.subtitles = this.dataset.subtitles;
		this.removeEventListener("click", playlistAdd);
		this.classList.remove("fa-plus");
		this.classList.add("fa-check");
		this.title = l("This video is in your playlist");
	}

	if (typeof this?.dataset?.index === 'string' || typeof this?.index === 'number') {
		// no double videos
		if (playlist.filter(search => search.title === video.title).length === 0) playlist[typeof this.index === 'number' ? this.index:parseInt(this.dataset.index)] = video;
	} else {
		// no double videos
		if (playlist.filter(search => search.title === video.title).length === 0) playlist.push(video);
	}

	let XNode = document.createElement("i");
		XNode.classList.add("fa", "fa-minus");
		XNode.addEventListener("click", playlistRemove);
		XNode.source = this.fakeEvent ? video.title:this;
	let TNode = document.createElement("span");
		TNode.innerText = l("{title} from {series}", {title: video.title, series: video.source});
		TNode.setAttribute('alt', l("{title} from {series}", {title: video.title, series: video.source}));
	let BNode = document.createElement("br");
	playlistContent.appendChild(XNode);
	playlistContent.appendChild(TNode);
	playlistContent.appendChild(BNode);

	savePlaylist();
	refreshPlaylistBox();
}

function playlistRemove() {
	for (let i = 0; i < playlist.length; ++i) {
		let id = window.config.USE_UID_AS_IDENTIFIER ? playlist[i].uid:playlist[i].file;
		let match;
		if (typeof this.source === 'string') {
			match = playlist[i].title === this.source;
		} else {
			url = new URLSearchParams(this.source.nextElementSibling.href.substring(this.source.nextElementSibling.href.indexOf("?")+1));
			match = id === url.get('video');
		}
		if (match) {
			playlist.splice(i,1);
			break;
		}
	}

	if (typeof this.source !== 'string') {
		this.title = l("Click to add this video to your playlist");
		this.source.classList.remove("fa-check");
		this.source.classList.add("fa-plus");
		this.source.addEventListener("click",  playlistAdd);
	}

	this.nextSibling.remove();
	this.nextSibling.remove();
	this.remove();

	savePlaylist();
	refreshPlaylistBox();
}

function editPlaylist() {
	let box = document.createElement("div");
		box.id = "box";
		box.innerHTML = "<p><span>" + l("Cancel") + "</span><span>" + l("Save") + "</span></p><textarea></textarea>";
		box.children[0].children[0].addEventListener("click", cancelEdit);
		box.children[0].children[1].addEventListener("click", loadPlaylist);

	for (let i = 0; i < playlist.length; ++i) {
		let id = window.config.USE_UID_AS_IDENTIFIER ? playlist[i].uid:playlist[i].file;
		box.children[1].value += id;
		if (i < playlist.length - 1) box.children[1].value += "\n";
	}

	document.body.appendChild(box);
}

function cancelEdit() {
	document.getElementById("box").remove();
}

async function loadPlaylist(fromPlaylistArray = false) {
	fromPlaylistArray = fromPlaylistArray === true;
	let X = playlistContent.getElementsByClassName("fa-minus");
	while (X.length) X[0].click();

	let identifers = [];

	if (fromPlaylistArray) {
		let i = 0;
		for (let video of playlist) {
			if (typeof video !== 'object' || video === null) { // failsafe for lost videos
				delete playlist[i];
			} else {
				identifers.push(window.config.USE_UID_AS_IDENTIFIER ? video.uid:video.file);
			}
			++i;
		}
		cleanPlaylist();
	} else {
		for (let source of document.getElementById('box').children[1].value.split("\n")) {
			identifers.push(source);
		}
	}

	console.log(identifers);

	let i = 0;
	for (let source of identifers) {
		source = source.trim();

		let j = 0;
		let videos = document.getElementsByClassName("video");
		while (j < videos.length) {
			let url = new URLSearchParams(videos[j].getAttribute("href").substring(videos[j].getAttribute("href").indexOf('?')+1));
			if (source === url.get('video')) {
				videos[j].previousElementSibling.dataset.index = i;
				videos[j].previousElementSibling.click();
				break;
			}

			++j;
		}

		if (j == videos.length) {
			// try to retrieve with api/details.php
			if (fromPlaylistArray) {
				playlistAdd.call({fakeEvent: true, index: null, video: playlist[i]});
			} else {
				let data = await fetch('/api/details.php?file='+encodeURIComponent(source)).then(r => {
					return r.json();
				});
				console.log(data);
				if (data.success) {
					// create a pseudo-event context
					playlistAdd.call({fakeEvent: true, index: i, video: {
						egg: null,
						title: data.title,
						source: data.source,
						file: data.filename,
						uid: data?.uid,
						mime: data.mime,
						song: data?.song,
						subtitles: data?.subtitles
					}});
				} else {
					// otherwise let the user know that we can't find the video
					let XNode = document.createElement("i");
						XNode.classList.add("fa", "fa-minus");
						XNode.addEventListener("click", function(){this.nextElementSibling.remove();this.remove();});
					let TNode = document.createElement("span");
						TNode.innerHTML = '<span>' + l('{id} could not be found.', {id: source}) + '</span>';
					let BNode = document.createElement("br");
					playlistContent.appendChild(XNode);
					playlistContent.appendChild(TNode);
					playlistContent.appendChild(BNode);
				}
			}
		}
		++i;
	}

	if (!fromPlaylistArray) document.getElementById('box').remove();
	cleanPlaylist();
	refreshPlaylistBox();
}

function startPlaylist() {
	mySessionStorage["videos"] = JSON.stringify(playlist);
	parent.history.pushState({video: playlist[0], index: 0}, "Custom Playlist", (getComputedStyle(document.querySelector("header")).display == "none") ? (showR18 ? "?r18" : "") : (showR18 ? "../?r18" : "../"));
	parent.history.go();
}
